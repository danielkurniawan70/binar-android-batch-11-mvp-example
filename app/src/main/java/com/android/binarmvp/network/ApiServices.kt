package com.android.binarmvp.network

import com.android.binarmvp.model.NewStudentResponse
import com.android.binarmvp.model.StudentResponse
import retrofit2.Call
import retrofit2.http.*

interface ApiServices {

    @GET("api/v1/student/all")
    fun getAllStudents(): Call<StudentResponse>

    @Headers("Content-Type: application/json")
    @POST("api/v1/student/")
    fun newStudent(@Body studentMap: Map<String, String>): Call<NewStudentResponse>

    @DELETE("api/v1/student/{studentId}")
    fun deleteStudent(@Path("studentId") id: Int): Call<NewStudentResponse>

    @PUT ("api/v1/student/{studentId}")
    fun editStudent (@Path("studentId") id: Int,
                     @Body map: Map<String, String>): Call<NewStudentResponse>
}