package com.android.binarmvp

import android.app.Application
import com.android.binarmvp.network.ApiServices
import com.android.binarmvp.network.NetworkConfig
import retrofit2.create

class BinarMvpApp : Application() {

    companion object {
        lateinit var api: ApiServices
    }

    override fun onCreate() {
        super.onCreate()

        api = NetworkConfig.getRetrofit().create(ApiServices::class.java)
    }
}