package com.android.binarmvp.model

import com.google.gson.annotations.SerializedName

data class ErrorResponse(
    @field:SerializedName("message")
    val message: String,

    @field:SerializedName("code")
    val code: Int
)