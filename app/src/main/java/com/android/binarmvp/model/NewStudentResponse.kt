package com.android.binarmvp.model

import com.google.gson.annotations.SerializedName

data class NewStudentResponse (
    @field:SerializedName("status")
    val status: String,

    @field:SerializedName("data")
    val data: Student?,

    @field:SerializedName("error")
    val error: ErrorResponse?
)