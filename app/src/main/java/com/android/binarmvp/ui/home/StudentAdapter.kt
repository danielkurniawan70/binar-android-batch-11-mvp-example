package com.android.binarmvp.ui.home

import android.support.v7.widget.RecyclerView
import android.text.Layout
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.android.binarmvp.R
import com.android.binarmvp.model.Student
import kotlinx.android.synthetic.main.item_student.view.*

class StudentAdapter(private val studentList: List<Student>,
                //higher order function handling on click
                     private val onClick: (Student) -> Unit,
                //higher order function handling on long click
                     private val onLongClick: (Student) -> Unit) : RecyclerView.Adapter<StudentAdapter.StudentHolder>() {

    override fun onCreateViewHolder(group: ViewGroup, type: Int): StudentHolder {
        val inflater = LayoutInflater.from(group.context)
        return StudentHolder(inflater.inflate(R.layout.item_student, group, false))
    }

    override fun getItemCount(): Int = studentList.size

    override fun onBindViewHolder(holder: StudentHolder, position: Int) {
        val student = studentList[position]
        holder.bind(student)
        holder.itemView.run{
            setOnClickListener {
                onClick(student)
            }
            setOnLongClickListener {
                onLongClick(student)
                return@setOnLongClickListener  true
            }
        }
    }

    inner class StudentHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(student: Student) = itemView.run {
            tvStudentName.text = student.name
            tvStudentEmail.text = student.email
        }
    }

}