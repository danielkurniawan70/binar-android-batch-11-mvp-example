package com.android.binarmvp.ui.newStudent

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.android.binarmvp.R
import com.android.binarmvp.common.Constant
import com.android.binarmvp.common.toast
import com.android.binarmvp.model.Student
import kotlinx.android.synthetic.main.activity_new_student.*

class FormStudentActivity : AppCompatActivity(), FormStudentView {

    private val presenter = FormStudentPresenter(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_student)


        setupView()
    }

    private fun setupView() {
        val student: Student? = intent.getParcelableExtra(Constant.STUDENT)
        btnSaveStudent.setOnClickListener {
            validateForm(student)
        }

        edtStudentName.setText(student?.name)
        edtStudentEmail.setText(student?.email)
        edtStudentName.setSelection(edtStudentName.length())
        edtStudentEmail.setSelection(edtStudentEmail.length())
    }

    private fun validateForm(student: Student?) {
        val name = edtStudentName.text.toString()
        val email = edtStudentEmail.text.toString()

        if (name.isBlank() || email.isBlank()) {
            toast("Nama dan email tidak boleh kosong")
            return
        }

        val map= mutableMapOf<String, String>()
        map["email"] = email
        map["name"] = name

        if (student == null) {
            presenter.newStudent(map)
        } else {
            presenter.editStudent(student.id, map)
        }

    }

    override fun onStudentSave(status: Boolean, message: String) {
        toast(message)
        if (status) {
            finish()
        }
    }
}