package com.android.binarmvp.ui.home

import com.android.binarmvp.BinarMvpApp
import com.android.binarmvp.model.NewStudentResponse
import com.android.binarmvp.model.Student
import com.android.binarmvp.model.StudentResponse
import com.android.binarmvp.network.ApiServices
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainPresenter(private val view: MainView) {

    private val apiServices: ApiServices by lazy {
        BinarMvpApp.api
    }

    fun getStudents() {
        view.showProgress(true)
        apiServices.getAllStudents()
            .enqueue(object : Callback<StudentResponse>{
                override fun onFailure(call: Call<StudentResponse>, t: Throwable) {
                    view.onError(t.localizedMessage)
                    view.showProgress(false)
                }

                override fun onResponse(call: Call<StudentResponse>, response: Response<StudentResponse>) {
                    view.showProgress(false)
                    if (response.body() !=null) {
                        val studentList = response.body()!!.data
                        studentList?.let {
                            view.showStudent(studentList)
                        }
                    } else {
                        view.onError("Error: Gagal memuat data")
                    }
                }

            })
    }

    fun deleteStudent(student: Student) {
            apiServices.deleteStudent(student.id)
                .enqueue(object : Callback<NewStudentResponse> {
                    override fun onFailure(call: Call<NewStudentResponse>, t: Throwable) {
                        view.onError(t.localizedMessage)
                        view.showProgress(false)
                    }

                    override fun onResponse(call: Call<NewStudentResponse>, response: Response<NewStudentResponse>) {
                        view.showProgress(false)
                        val body: NewStudentResponse? = response.body()
                        if (body?.status == "OK") {
                            view.onDeleteStudent(student,true,"Berhasil menghapus siswa ${student.name}")
                        } else {
                            view.onDeleteStudent(student,false, "Error: Gagal menghapus data siswa ${student.name}")
                        }
                    }
                })
    }
}
