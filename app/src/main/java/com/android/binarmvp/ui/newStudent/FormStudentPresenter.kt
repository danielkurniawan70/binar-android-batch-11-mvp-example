package com.android.binarmvp.ui.newStudent

import com.android.binarmvp.BinarMvpApp
import com.android.binarmvp.model.NewStudentResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class FormStudentPresenter(private val view: FormStudentView) {
    fun newStudent(studentMap: Map<String, String>) {
        BinarMvpApp.api.newStudent(studentMap)
            .enqueue(object : Callback<NewStudentResponse>{
                override fun onFailure(call: Call<NewStudentResponse>, t: Throwable) {
                    view.onStudentSave(false, t.localizedMessage)
                }

                override fun onResponse(call: Call<NewStudentResponse>, response: Response<NewStudentResponse>) {
                    val isSuccess = response.body()?.status == "OK"
                    if (isSuccess)
                        view.onStudentSave(isSuccess, "Berhasil menyimpan data siswa")
                    else
                        view.onStudentSave(isSuccess, "Gagal menyimpan data siswa, coba lagi")
                }
            })
    }

    fun editStudent(studentId: Int, map: Map<String, String>) {
        BinarMvpApp.api.editStudent(studentId, map)
            .enqueue(object : Callback<NewStudentResponse>{
                override fun onFailure(call: Call<NewStudentResponse>, t: Throwable) {
                    view.onStudentSave(false, t.localizedMessage)
                }

                override fun onResponse(call: Call<NewStudentResponse>, response: Response<NewStudentResponse>) {
                    val isSuccess = response.body()?.status == "OK"
                    if (isSuccess)
                        view.onStudentSave(isSuccess, "Berhasil mengubah data siswa")
                    else
                        view.onStudentSave(isSuccess, "Gagal mengubah data siswa, coba lagi")
                }

            })
    }

}