package com.android.binarmvp.ui.home

import com.android.binarmvp.model.Student

interface MainView {

    fun showStudent(results: List<Student>)
    fun onError(message: String)
    fun onDeleteStudent(student: Student, isSuccess: Boolean, s: String)
    fun showProgress(show: Boolean)

}